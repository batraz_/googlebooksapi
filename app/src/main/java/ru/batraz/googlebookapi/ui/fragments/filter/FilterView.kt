package ru.batraz.googlebookapi.ui.fragments.filter

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import ru.batraz.googlebookapi.adapters.FilterAdapter

@AddToEndSingle
interface FilterView: MvpView {
    fun setAdapter(adapter: FilterAdapter)
}