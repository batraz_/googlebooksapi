package ru.batraz.googlebookapi.ui.fragments.book_list

import android.content.Context
import android.util.Log
import com.github.terrakok.cicerone.Router
import dagger.hilt.android.qualifiers.ActivityContext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.MvpPresenter
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.adapters.BooksAdapter
import ru.batraz.googlebookapi.models.common.Book
import ru.batraz.googlebookapi.repositories.GoogleBooksRepository
import ru.batraz.googlebookapi.ui.fragments.book_detail.BookDetailFragment
import ru.batraz.googlebookapi.utils.AppBarDelegate
import ru.batraz.googlebookapi.utils.AppBarMode
import ru.batraz.googlebookapi.utils.BooksFilter
import ru.batraz.googlebookapi.utils.BooksLoadingMode
import javax.inject.Inject


class BookListPresenter @Inject constructor(
    @ActivityContext private val  activityContext: Context,
    private val googleBooksRepository: GoogleBooksRepository,
    private val router: Router,
    private val filter: BooksFilter,
    private val appBarDelegate: AppBarDelegate
): MvpPresenter<BookListView>(), BooksAdapter.Delegate {

    private val disposable: CompositeDisposable = CompositeDisposable()
    private val adapter: BooksAdapter = BooksAdapter()
    private var loadSearchQuery: String? = null

    init {
        adapter.delegate = this
        disposable.add(googleBooksRepository.searchedBooks.subscribe({
            adapter.items = it
            adapter.notifyDataSetChanged()

            viewState.setPlaceholderVisibility(it.size == 0)
        }, {}))

        disposable.add(googleBooksRepository
            .loadingState
            .subscribe({
                Log.d("LOADING", "${it.name}")
                viewState.setProgressVisibility(it == BooksLoadingMode.LoadWithNewQuery)

                if (it == BooksLoadingMode.LoadNextPage) {
                    adapter.addLoading()
                } else {
                    adapter.removeLoading()
                }
            }, {})
        )
    }


    //  MVP
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.setAdapter(adapter)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    override fun attachView(view: BookListView?) {
        super.attachView(view)
        appBarDelegate.setAppBarStyle(AppBarMode.Search)
        appBarDelegate.setTitle(R.string.app_bar_title_empty)
    }



    //  BOOKS ADAPTER DELEGATE
    override fun onSelect(book: Book) {
        router.navigateTo(BookDetailFragment.Screen(book))
    }


    //  OTHERS
    fun scrollToLastItem() {
        val loadingState = googleBooksRepository.loadingState.value ?: return

        if (loadingState == BooksLoadingMode.NoLoading && !adapter.isLoading) {
            adapter.addLoading()
            googleBooksRepository.loadNext(filter)
        }
    }

}