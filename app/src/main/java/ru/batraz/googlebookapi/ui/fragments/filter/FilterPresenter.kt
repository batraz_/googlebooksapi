package ru.batraz.googlebookapi.ui.fragments.filter

import moxy.MvpPresenter
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.adapters.FilterAdapter
import ru.batraz.googlebookapi.utils.AppBarDelegate
import ru.batraz.googlebookapi.utils.BooksFilter
import ru.batraz.googlebookapi.utils.Filter
import ru.batraz.googlebookapi.utils.AppBarMode
import javax.inject.Inject


class FilterPresenter @Inject constructor(
    val filter: BooksFilter,
    val appBarDelegate: AppBarDelegate
): MvpPresenter<FilterView>(), FilterAdapter.Delegate {
    private val adapter: FilterAdapter = FilterAdapter()


    init {
        adapter.delegate = this
    }


    //  MVP
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        adapter.items = filter.getFilterFields()
        adapter.selectedFilter = filter.selectedFilter.value ?: Filter.Null

        viewState.setAdapter(adapter)
    }

    override fun attachView(view: FilterView?) {
        super.attachView(view)
        appBarDelegate.setAppBarStyle(AppBarMode.TitleWithBackButton)
        appBarDelegate.setTitle(R.string.app_bar_title_search_config)
    }


    //  ADAPTER DELEGATE
    override fun selectFilter(filter: Filter) {
        this.filter.selectFilter(filter)
        adapter.selectedFilter = filter
        adapter.notifyDataSetChanged()
    }


}