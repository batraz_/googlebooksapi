package ru.batraz.googlebookapi.ui.fragments.book_detail

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import butterknife.BindView
import butterknife.ButterKnife
import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatFragment
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.models.common.Book
import ru.batraz.googlebookapi.utils.AppBarDelegate
import ru.batraz.googlebookapi.utils.AppBarMode
import javax.inject.Inject

@SuppressLint("NonConstantResourceId")
@AndroidEntryPoint
class BookDetailFragment: MvpAppCompatFragment() {

    @Inject
    lateinit var appBarDelegate: AppBarDelegate

    @BindView(R.id.ivPicture)
    lateinit var ivPicture: ImageView

    @BindView(R.id.tvTitle)
    lateinit var tvTitle: TextView

    @BindView(R.id.tvAuthor)
    lateinit var tvAuthor: TextView

    @BindView(R.id.tvDetails)
    lateinit var tvDetails: TextView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_book_detail, container, false)
        ButterKnife.bind(this, view)

        setContent()
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        appBarDelegate.setAppBarStyle(AppBarMode.TitleWithBackButton)
        appBarDelegate.setTitle(R.string.app_bar_title_search_config)
    }

    private fun setContent() {
        (arguments?.getSerializable(MODEL_KEY) as? Book)?.let { model ->
            tvTitle.text = model.title
            tvAuthor.text = model.authors.joinToString { v -> "$v\n" }
            tvDetails.text = model.description
            model.imageLinks?.thumbnail?.let { imgUrl ->
                Picasso.get()
                    .load(imgUrl)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(ivPicture)
            }
        }
    }




    companion object {
        private const val MODEL_KEY = "model"
    }

    class Screen(val book: Book): FragmentScreen {
        override fun createFragment(factory: FragmentFactory): Fragment {
            val fragment = BookDetailFragment()
            val args = Bundle()
            args.putSerializable(MODEL_KEY, book)

            fragment.arguments = args

            return fragment
        }

    }
}