package ru.batraz.googlebookapi.ui.fragments.filter

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.adapters.BooksAdapter
import ru.batraz.googlebookapi.adapters.FilterAdapter
import ru.batraz.googlebookapi.ui.fragments.book_detail.BookDetailFragment
import javax.inject.Inject
import javax.inject.Provider

@SuppressLint("NonConstantResourceId")
@AndroidEntryPoint
class FilterFragment: MvpAppCompatFragment(), FilterView {

    @Inject
    lateinit var presenterProvider: Provider<FilterPresenter>
    val presenter: FilterPresenter by moxyPresenter { presenterProvider.get() }

    @BindView(R.id.recycler)
    lateinit var recycler: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = layoutInflater.inflate(R.layout.fragment_filter, container, false)
        ButterKnife.bind(this, view)

        return view
    }

    //  MVP
    override fun setAdapter(adapter: FilterAdapter) {
        recycler.adapter = adapter
    }




    class Screen: FragmentScreen {
        override fun createFragment(factory: FragmentFactory): Fragment {
            return FilterFragment()
        }
    }
}