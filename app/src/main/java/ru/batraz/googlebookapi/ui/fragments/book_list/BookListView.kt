package ru.batraz.googlebookapi.ui.fragments.book_list

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution
import ru.batraz.googlebookapi.adapters.BooksAdapter

@AddToEndSingle
interface BookListView: MvpView {

    fun setAdapter(adapter: BooksAdapter)

    fun setText(text: String)

    fun setPlaceholderVisibility(isVisible: Boolean)

    fun setProgressVisibility(isVisible: Boolean)

}