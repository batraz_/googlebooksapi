package ru.batraz.googlebookapi.ui.main

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution
import ru.batraz.googlebookapi.utils.AppBarMode

@AddToEndSingle
interface MainActivityView: MvpView {

    @OneExecution
    fun changeMenuMode(mode: AppBarMode, title: String? = null)

    @OneExecution
    fun setFilterIcon(withAppliedFilter: Boolean)

    @OneExecution
    fun setSearchField(searchValue: String)

    fun setAppBarTitle(title: String)

}