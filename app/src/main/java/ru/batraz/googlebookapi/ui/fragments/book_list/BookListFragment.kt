package ru.batraz.googlebookapi.ui.fragments.book_list

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.github.terrakok.cicerone.androidx.FragmentScreen
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.adapters.BooksAdapter
import ru.batraz.googlebookapi.utils.visibility
import javax.inject.Inject
import javax.inject.Provider


@SuppressLint("NonConstantResourceId")
@AndroidEntryPoint
class BookListFragment: MvpAppCompatFragment(), BookListView {

    @Inject
    lateinit var presenterProvider: Provider<BookListPresenter>
    val presenter: BookListPresenter by moxyPresenter { presenterProvider.get() }

    @BindView(R.id.recycler)
    lateinit var recycler: RecyclerView

    @BindView(R.id.tvPlaceholder)
    lateinit var tvPlaceholder: TextView

    @BindView(R.id.progress)
    lateinit var progress: ProgressBar



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_book_list, container, false)
        ButterKnife.bind(this, view)
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val layoutManager: LinearLayoutManager = recycler.layoutManager as LinearLayoutManager

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= 10) {
                    activity?.runOnUiThread {
                        presenter.scrollToLastItem()
                    }
                }

            }
        })

        return view
    }

    override fun setAdapter(adapter: BooksAdapter) {
        recycler.adapter = adapter
        setPlaceholderVisibility(adapter.items.size == 0)
    }

    override fun setText(text: String) {
        tvPlaceholder.text = text
    }

    override fun setPlaceholderVisibility(isVisible: Boolean) {
        tvPlaceholder.visibility = isVisible.visibility
    }

    override fun setProgressVisibility(isVisible: Boolean) {
        progress.visibility = isVisible.visibility
    }




    class Screen: FragmentScreen {
        override fun createFragment(factory: FragmentFactory): Fragment {
            return BookListFragment()
        }
    }



}