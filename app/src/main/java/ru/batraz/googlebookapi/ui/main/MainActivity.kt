package ru.batraz.googlebookapi.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import com.github.terrakok.cicerone.*
import com.github.terrakok.cicerone.androidx.AppNavigator
import dagger.hilt.android.AndroidEntryPoint
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.utils.AppBarMode
import ru.batraz.googlebookapi.utils.AppBarMode.*
import ru.batraz.googlebookapi.utils.BooksFilter
import javax.inject.Inject
import javax.inject.Provider

@AndroidEntryPoint
class MainActivity: MvpAppCompatActivity(R.layout.activity_main), MainActivityView, SearchView.OnQueryTextListener {

    @Inject
    lateinit var presenterProvider: Provider<MainActivityPresenter>
    private val presenter: MainActivityPresenter by moxyPresenter { presenterProvider.get() }

    @Inject
    lateinit var filter: BooksFilter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    private val navigator = AppNavigator(this, R.id.container)
    private var menuItemSearch: MenuItem? = null
    private var menuItemFilter: MenuItem? = null
    private var searchView: SearchView? = null

    private var searchSavedValue: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchSavedValue = savedInstanceState?.getString(KEY_SEARCH)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (menu == null) return super.onCreateOptionsMenu(menu)

        menuInflater.inflate(R.menu.main_menu, menu)

        //  Config search
        menuItemSearch = menu.findItem(R.id.search)

        searchView = (menuItemSearch?.actionView as? SearchView)
        searchView?.queryHint = resources.getString(R.string.search_title)
        searchView?.setIconifiedByDefault(false)
        searchView?.setOnQueryTextListener(this)


        //  Config filter
        menuItemFilter = menu.findItem(R.id.filter)
        menuItemFilter?.setOnMenuItemClickListener(this::onFilterSelected)

        //  Config back pressed callback
        findViewById<Toolbar>(androidx.appcompat.R.id.action_bar)?.let {
            it.setNavigationOnClickListener(this::backAppBarButtonSelected)
        }

        title = "Параметры поиска"

        presenter.appBarItemsInitialized()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(KEY_SEARCH, searchView?.query?.toString())
    }


    //  MVP VIEW METHODS
    override fun changeMenuMode(mode: AppBarMode, title: String?) {
        when(mode) {
            Search -> {
                menuItemSearch?.isVisible = true
                menuItemFilter?.isVisible = true

                supportActionBar?.setDisplayHomeAsUpEnabled(false)
                supportActionBar?.setDisplayShowHomeEnabled(false)
                setTitle("")
            }
            TitleWithBackButton -> {
                menuItemSearch?.isVisible = false
                menuItemFilter?.isVisible = false
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                supportActionBar?.setDisplayShowHomeEnabled(true)
                setTitle("Параметры поиска")
            }
        }
    }

    override fun setFilterIcon(withAppliedFilter: Boolean) {
        menuItemFilter?.setIcon(if (withAppliedFilter) R.drawable.ic_filter_selected else R.drawable.ic_filter)
    }

    override fun setSearchField(searchValue: String) {
        (menuItemSearch?.actionView as? SearchView)?.setQuery(searchValue, false)
    }

    override fun setAppBarTitle(title: String) {
        this.title = title
    }


    //  QUERY TEXT LISTENER
    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let { searchValue ->
            presenter.searchFieldUpdated(searchValue)
        }

        return true
    }


    //  VIEW LISTENERS
    private fun onFilterSelected(menuItem: MenuItem?): Boolean {
        presenter.filterSelected()
//        navigator.applyCommands(arrayOf(Forward()))
//        navigator.applyCommands(arrayOf(Forward()))

        return true
    }

    private fun backAppBarButtonSelected(view: View?) {
        presenter.backPressed()
//        Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show()
//        navigator.applyCommands(arrayOf(Back()))
    }



    companion object {
        const val KEY_SEARCH = "search"
        const val KEY_IS_FILTERED = "filter"
    }

}