package ru.batraz.googlebookapi.ui.main

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import moxy.MvpPresenter
import ru.batraz.googlebookapi.repositories.GoogleBooksRepository
import ru.batraz.googlebookapi.ui.fragments.book_list.BookListFragment
import ru.batraz.googlebookapi.ui.fragments.filter.FilterFragment
import ru.batraz.googlebookapi.utils.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainActivityPresenter @Inject constructor(
    val router: Router,
    val cicerone: Cicerone<Router>,
    val filter: BooksFilter,
    val appBarDelegate: AppBarDelegate
    ) : MvpPresenter<MainActivityView>(), AppBar {

    @Inject
    lateinit var googleRepository: GoogleBooksRepository

    private val searchValue: BehaviorSubject<String> = BehaviorSubject.create()
    private val disposable: CompositeDisposable = CompositeDisposable()
    private var lastAppBarTitle: String? = null
    private var lastAppBarStyle: AppBarMode? = null

    init {
        appBarDelegate.attach(this)
        val d1 = searchValue
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .subscribe({
                googleRepository.search(it, filter)
            }, {})


        val d2 = filter.selectedFilter
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when (it) {
                    Filter.Null -> viewState.setFilterIcon(false)
                    else -> viewState.setFilterIcon(true)
                }

                searchValue.value?.let { searchQuery ->
                    if (searchQuery.isNotEmpty()) {
                        searchValue.onNext(searchQuery)
                    }
                }

            }, {
                filter.selectFilter(Filter.Null)
            })


    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        val fragment = BookListFragment.Screen()


        router.newRootScreen(fragment)
    }

    fun searchFieldUpdated(newValue: String) {
        searchValue.onNext(newValue)
    }

    fun backPressed() {
        router.backTo(BookListFragment.Screen())
    }

    fun filterSelected() {
        router.navigateTo(FilterFragment.Screen())
    }

    fun appBarItemsInitialized() {
        searchValue.value?.let { searchValue -> viewState.setSearchField(searchValue) }
        viewState.setFilterIcon(filter.selectedFilter.value != Filter.Null)
        lastAppBarTitle?.let { appBarTitle -> viewState.setAppBarTitle(appBarTitle) }
        lastAppBarStyle?.let { appBarStyle -> viewState.changeMenuMode(appBarStyle) }
    }



    // APP BAR DELEGATE
    override fun setAppBarStyle(style: AppBarMode) {
        lastAppBarStyle = style
        viewState.changeMenuMode(style)
    }

    override fun setAppBarTitle(title: String) {
        lastAppBarTitle = title
        viewState.setAppBarTitle(title)
    }




    override fun onDestroy() {
        super.onDestroy()
        appBarDelegate.detach()
        disposable.dispose()
    }
}
