package ru.batraz.googlebookapi.utils

enum class AppBarMode {
    Search,
    TitleWithBackButton
}