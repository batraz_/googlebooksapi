package ru.batraz.googlebookapi.utils

import android.content.Context
import androidx.annotation.StringRes

class AppBarDelegate(val context: Context?) {

    private var appBar: AppBar? = null

    fun attach(appBar: AppBar) {
        this.appBar = appBar
    }

    fun detach() {
        appBar = null
    }

    fun setAppBarStyle(style: AppBarMode) {
        appBar?.setAppBarStyle(style)
    }

    fun setTitle(title: String) {
        appBar?.setAppBarTitle(title)
    }

    fun setTitle(@StringRes titleResID: Int) {
        val title = context?.getString(titleResID) ?: ""
        setTitle(title)
    }

}

interface AppBar {
    fun setAppBarStyle(style: AppBarMode)
    fun setAppBarTitle(title: String)
}
