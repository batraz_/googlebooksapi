package ru.batraz.googlebookapi.utils

enum class BooksLoadingMode {
    NoLoading,
    LoadWithNewQuery,
    LoadNextPage
}