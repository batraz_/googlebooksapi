package ru.batraz.googlebookapi.utils

import io.reactivex.subjects.BehaviorSubject

class BooksFilter {
    val selectedFilter: BehaviorSubject<Filter> = BehaviorSubject.createDefault(Filter.Null)

    fun getFilterFields(): ArrayList<Filter> {
        return ArrayList(Filter.values().toMutableList())
    }

    fun selectFilter(filter: Filter) {
        if (selectedFilter.value != filter) selectedFilter.onNext(filter)
    }

}

enum class Filter(val value: String){
    Null("Поиск по всему"),
    ByAuthor("Поиск по автору"),
    ByName("Поиск по названию"),
    ByGenre("Поиск по жанру"),
    ByPublisher("Поиск по издателю");


    val requestField: String?
        get() {
            return when (this) {
                Null -> null
                ByAuthor -> "inauthor"
                ByName -> "intitle"
                ByGenre -> "subject"
                ByPublisher -> "inpublisher"
            }
        }
}