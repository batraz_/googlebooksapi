package ru.batraz.googlebookapi.utils

import android.view.View

//  Visible extension
val Boolean.visibility: Int
    get() {
        return if (this) View.VISIBLE else View.GONE
    }