package ru.batraz.googlebookapi.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.models.common.Book
import ru.batraz.googlebookapi.utils.visibility

class BooksAdapter: RecyclerView.Adapter<BooksAdapter.BaseViewHolder>() {

    var items: ArrayList<Book> = ArrayList()
    var delegate: Delegate? = null
    var isLoading = false

    interface Delegate {
        fun onSelect(book: Book)
    }


    //  ADAPTER
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when(viewType) {
            ViewType.Progress.type -> {
                val resViewId = R.layout.item_loading
                val view = LayoutInflater.from(parent.context).inflate(resViewId, parent, false)

                ProgressViewHolder(view)
            }
            else -> {
                val resViewId = R.layout.item_book
                val view = LayoutInflater.from(parent.context).inflate(resViewId, parent, false)

                ViewHolder(view)
            }
        }


    }

    override fun onBindViewHolder(holder: BooksAdapter.BaseViewHolder, position: Int) {
        holder.bind(items[position])
        holder.isLastItem(position == items.size - 1)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        val isLoadType = items[position].modelType == Book.ModelType.LoadingPlaceholder

        return if (isLoadType) ViewType.Progress.type else ViewType.Item.type
    }


    //  LOADING
    fun addLoading() {
        if (isLoading) return

        isLoading = true
        items.add(Book().apply { modelType = Book.ModelType.LoadingPlaceholder })
        notifyItemInserted(items.size - 1)
    }

    fun removeLoading() {
        if (isLoading) {
            val loadItemIndex = items.indexOfFirst { v -> v.modelType == Book.ModelType.LoadingPlaceholder }
            if (loadItemIndex >= 0) {
                items.removeAt(loadItemIndex)
                notifyItemRemoved(loadItemIndex)
            }

            isLoading = false
        }
    }


    abstract class BaseViewHolder(view: View): RecyclerView.ViewHolder(view) {
        abstract fun bind(model: Book)
        abstract fun isLastItem(isLast: Boolean)
    }


    inner class ViewHolder(view: View): BaseViewHolder(view) {
        private var model: Book? = null

        private val ivPicture: ImageView = view.findViewById(R.id.ivPicture)
        private val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        private val tvAuthor: TextView = view.findViewById(R.id.tvAuthor)
        private val viewSeparator: View = view.findViewById(R.id.viewSeparator)


        init {
            view.setOnClickListener {
                model?.let { model ->
                    delegate?.onSelect(model)
                }
            }
        }

        override fun bind(model: Book) {
            this.model = model

            tvAuthor.text = model.authors.joinToString { v -> "$v\n" }
            tvTitle.text = model.title

            model.imageLinks?.thumbnail.let { imgLink ->
                Picasso.get()
                    .load(imgLink)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(ivPicture)
            }


        }

        override fun isLastItem(isLast: Boolean) {
            viewSeparator.visibility = (!isLast).visibility
        }
    }

    inner class ProgressViewHolder(view: View): BaseViewHolder(view){

        override fun bind(model: Book) {
        }

        override fun isLastItem(isLast: Boolean) {
        }

    }

    enum class ViewType(val type: Int) {
        Item(1),
        Progress(2);
    }


}