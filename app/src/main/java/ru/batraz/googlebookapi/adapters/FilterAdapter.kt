package ru.batraz.googlebookapi.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.batraz.googlebookapi.R
import ru.batraz.googlebookapi.utils.BooksFilter
import ru.batraz.googlebookapi.utils.Filter
import ru.batraz.googlebookapi.utils.visibility

class FilterAdapter: RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    interface Delegate {
        fun selectFilter(filter: Filter)
    }

    var delegate: Delegate? = null
    var items: ArrayList<Filter> = ArrayList()
    var selectedFilter: Filter = Filter.Null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_filter, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.setLast(position == items.size - 1)
    }

    override fun getItemCount(): Int = items.size



    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private var model: Filter? = null

        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val separator: View = view.findViewById(R.id.separator)
        val ivSelected: ImageView = view.findViewById(R.id.ivSelected)

        init {
            view.setOnClickListener {
                model?.let { filter ->
                    delegate?.selectFilter(filter)
                }
            }
        }

        fun bind(model: Filter) {
            this.model = model

            tvTitle.text = model.value
            ivSelected.visibility = (model == selectedFilter).visibility
        }

        fun setLast(isLast: Boolean) {
            separator.visibility = (!isLast).visibility
        }


    }

}