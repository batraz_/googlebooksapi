package ru.batraz.googlebookapi.core

import android.app.Application
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.androidx.AppNavigator
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.internal.managers.ViewComponentManager

@HiltAndroidApp
class App: Application()