package ru.batraz.googlebookapi.models.network

class SearchBooksResponse {
    var kind: String? = null
    var totalItems: Int? = null
    var items: ArrayList<BookItem> = ArrayList()
}