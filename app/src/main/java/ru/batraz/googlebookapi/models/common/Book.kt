package ru.batraz.googlebookapi.models.common

import java.io.Serializable

class Book: Serializable {
    var title: String? = null
    var authors: ArrayList<String> = ArrayList()
    var publisher: String? = null
    var publishedDate: String? = null
    var description: String? = null
    var imageLinks: BookImages? = null
    var previewLink: String? = null
    var infoLink: String? = null
    var canonicalVolumeLink: String? = null

    var modelType: ModelType = ModelType.Book

    class BookImages {
        var smallThumbnail: String? = null
        var thumbnail: String? = null
        var small: String? = null
        var medium: String? = null
        var large: String? = null
        var extraLarge: String? = null
    }

    enum class ModelType {
        Book,
        LoadingPlaceholder
    }

}