package ru.batraz.googlebookapi.models.network

import ru.batraz.googlebookapi.models.common.Book

class BookItem {
    var kind: String? = null
    var id: String? = null
    var etag: String? = null
    var selfLink: String? = null
    var volumeInfo: Book? = null
}