package ru.batraz.googlebookapi.repositories

import android.annotation.SuppressLint
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import ru.batraz.googlebookapi.models.common.Book
import ru.batraz.googlebookapi.models.network.SearchBooksResponse
import ru.batraz.googlebookapi.services.GoogleService
import ru.batraz.googlebookapi.utils.BooksFilter
import ru.batraz.googlebookapi.utils.BooksLoadingMode
import ru.batraz.googlebookapi.utils.Filter

class GoogleBooksRepository(private val googleService: GoogleService) {

    val searchedBooks: BehaviorSubject<ArrayList<Book>> = BehaviorSubject.create()
    val loadingState: BehaviorSubject<BooksLoadingMode> = BehaviorSubject.createDefault(BooksLoadingMode.NoLoading)

    private val count: Int
        get() = searchedBooks.value?.size ?: 0

    private var lastSearchQuery: String? = null
    private var lastSearchFilter: Filter = Filter.Null



    @SuppressLint("CheckResult")
    fun search(searchQuery: String, filter: BooksFilter) {
        Log.d("REPOSITORY", "Query: $searchQuery; Now ${searchedBooks.value?.size ?: 0} items")
        if (loadingState.value != BooksLoadingMode.NoLoading) return

        val filterParam = filter.selectedFilter.value ?: Filter.Null

        if (lastSearchQuery != searchQuery || filterParam != lastSearchFilter) {
            loadingState.onNext(BooksLoadingMode.LoadWithNewQuery)
            searchedBooks.onNext(ArrayList())
            Log.d("REPOSITORY", "LoadWithNewQuery")
        } else {
            loadingState.onNext(BooksLoadingMode.LoadNextPage)
            Log.d("REPOSITORY", "LoadNextPage")
        }

        lastSearchQuery = searchQuery


        when (filterParam) {
            Filter.Null -> {
                googleService.searchBooks(searchQuery, count)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleSuccess, this::handleFail, this::handleComplete)
            }
            else ->  {
                googleService.searchBookWithFilter(searchQuery, filterParam, count)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleSuccess, this::handleFail, this::handleComplete)
            }
        }
    }

    fun loadNext(filter: BooksFilter) {
        lastSearchQuery?.let { searchQuery ->
            search(searchQuery, filter)
        }
        Log.d("REPOSITORY", "Load next")

    }


    //  RESPONSE HANDLING
    private fun handleSuccess(response: SearchBooksResponse) {
        val books = searchedBooks.value ?: ArrayList()
        books.addAll(response.items.mapNotNull { v -> v.volumeInfo })

        searchedBooks.onNext(books)
    }

    private fun handleFail(it: Throwable) {
        searchedBooks.onNext(ArrayList())
        loadingState.onNext(BooksLoadingMode.NoLoading)
    }

    private fun handleComplete() {
        loadingState.onNext(BooksLoadingMode.NoLoading)
    }

}