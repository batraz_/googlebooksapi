package ru.batraz.googlebookapi.services

import io.reactivex.Observable
import ru.batraz.googlebookapi.models.network.SearchBooksResponse
import ru.batraz.googlebookapi.network.GoogleApi
import ru.batraz.googlebookapi.utils.Filter

class GoogleService constructor(
    private val googleApi: GoogleApi
) {

    fun searchBooks(searchQuery: String, startIndex: Int): Observable<SearchBooksResponse> {
        return googleApi.searchBooks(searchQuery, startIndex)
    }

    fun searchBookWithFilter(searchQuery: String, filterType: Filter, startIndex: Int): Observable<SearchBooksResponse> {
        filterType.requestField?.let {
            return googleApi.searchBooks("$it:$searchQuery")
        }

        return searchBooks(searchQuery, startIndex)
    }

}