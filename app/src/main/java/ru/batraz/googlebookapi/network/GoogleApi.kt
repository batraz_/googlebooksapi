package ru.batraz.googlebookapi.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ru.batraz.googlebookapi.models.network.SearchBooksResponse

interface GoogleApi {

    @GET("books/v1/volumes")
    fun searchBooks(
        @Query("q") searchQuery: String,
        @Query("startIndex") startIndex: Int? = null
    ): Observable<SearchBooksResponse>


    companion object {
        const val baseUrl: String = "https://www.googleapis.com"
    }
}