package ru.batraz.googlebookapi.di.modules

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import ru.batraz.googlebookapi.utils.BooksFilter
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FilterModule {

    @Provides
    @Singleton
    fun provideFilter(): BooksFilter {
        return BooksFilter()
    }

}