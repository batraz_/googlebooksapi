package ru.batraz.googlebookapi.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import ru.batraz.googlebookapi.utils.AppBarDelegate
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SingleActivityModule {

    @Provides
    @Singleton
    fun provideAppBarDelegate(@ApplicationContext context: Context): AppBarDelegate {
        return AppBarDelegate(context)
    }
}