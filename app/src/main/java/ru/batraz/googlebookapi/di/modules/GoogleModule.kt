package ru.batraz.googlebookapi.di.modules

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import ru.batraz.googlebookapi.network.GoogleApi
import ru.batraz.googlebookapi.repositories.GoogleBooksRepository
import ru.batraz.googlebookapi.services.GoogleService
import ru.batraz.googlebookapi.utils.BooksFilter
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module(includes = [RetrofitModule::class])
object GoogleModule {

    @Singleton
    @Provides
    fun provideGoogleApi(retrofit: Retrofit): GoogleApi {
        return retrofit.create(GoogleApi::class.java)
    }

    @Singleton
    @Provides
    fun provideGoogleService(api: GoogleApi): GoogleService {
        return GoogleService(api)
    }

    @Singleton
    @Provides
    fun provideGoogleRepository(service: GoogleService): GoogleBooksRepository {
        return GoogleBooksRepository(service)
    }

}